from . import db
from datetime import date

class Users(db.Model):
	__tablename__ = 'users'
	id = db.Column(db.Integer, primary_key=True)
	fname = db.Column(db.String(64), nullable=False)
	sname = db.Column(db.String(64), nullable=True)
	dateofbirth = db.Column(db.Date, nullable=False)
	timestamp = db.Column(db.DateTime, nullable=False)

	def days_alive(self):
		td = date.today() - self.dateofbirth
		return td.days

	def days_alive_mars(self):
		td = date.today() - self.dateofbirth
		return int(td.days / 1.02749)
