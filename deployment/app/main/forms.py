from flask.ext.wtf import Form
from wtforms import StringField, SubmitField
from wtforms.fields.html5 import DateField
from wtforms.validators import Required


class DateForm(Form):
	fname = StringField('What is your first name?', validators=[Required()])
	sname = StringField('Surname:')
	dt = DateField('Birthdate:', format='%Y-%m-%d', validators=[Required()])
	submit = SubmitField('Submit')