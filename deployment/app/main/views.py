from datetime import datetime
from flask import render_template, session, redirect, url_for
from . import main
from .forms import DateForm
from .. import db
from ..models import Users

@main.route('/', methods=['GET', 'POST'])
def input():
	form = DateForm()
	if form.validate_on_submit():
		fname = form.fname.data
		sname = form.sname.data
		date = form.dt.data
		user = Users(fname=fname, sname=sname, dateofbirth=date, timestamp=datetime.now())
		db.session.add(user)
		db.session.commit()

		return render_template('output.html', fname=fname, sname=sname, days_alive=user.days_alive(),
								days_alive_mars=user.days_alive_mars())

	return render_template('input.html', form=form)


@main.route('/list')
def list():
	results = Users.query.all()
	return render_template("list.html", results=results)